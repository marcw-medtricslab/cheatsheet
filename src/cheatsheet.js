// Constants
let MT_jira_base = "https://medtricslab.atlassian.net/browse/";
let MT_gitlab_issue = "https://gitlab.com/medtrics/medtrics/-/issues/";

// @todo make a complex nested dictionary and iterate as a function.  Probably using data
// @todo attributes, at least 20 lines of additional code, and a brittle hard to grok
// schema to be fancy and not effective.

// ON enter for a particular input
document.getElementById("MT_jira_ops").addEventListener("keyup", function (event) {
    event.preventDefault();
    if (event.key === "Enter") {
        window.location.assign(MT_jira_base + "OPS-" + document.getElementById("MT_jira_ops").value);
    }
});

document.getElementById("MT_jira_dev").addEventListener("keyup", function (event) {
    event.preventDefault();
    if (event.key === "Enter") {
        window.location.assign(MT_jira_base + "DV-" + document.getElementById("MT_jira_dev").value);
    }
});

document.getElementById("MT_gitlab_issue").addEventListener("keyup", function (event) {
    event.preventDefault();
    if (event.key === "Enter") {
        window.location.assign(MT_gitlab_issue + document.getElementById("MT_gitlab_issue").value);
    }
});

// input and link highlighting

let sexy_elements = document.getElementsByClassName("make_sexy");

for (let i = 0; i < sexy_elements.length; i++) {
    sexy_elements[i].addEventListener(
        "focus", function(){mt_onfocus( sexy_elements[i]);});
    sexy_elements[i].addEventListener("blur", function(){
        mt_onblur(sexy_elements[i]);});
}

function mt_onfocus(ele) {
    ele.style.color = "rgb(255, 255, 255)";
    ele.style.backgroundColor = "rgb(255,0,255)";
}

function mt_onblur(ele) {
    ele.style.color = "rgb(25,29,255)";
    ele.style.backgroundColor = "rgb(255,255,255)";
}
